# Jakarta EE 10 Web Quickstart
**Runtime**
- [Available Runtimes](#run)

**Features**
- [**Overview**](#platform-overview)
- [Persistence](#jakarta-persistence)
- [RESTful Web Services](#jakarta-rest)
- [Server Faces](#jakarta-server-faces)
## Build
`mvn clean package`

Package the application as Vendor independent WAR.
## Run
Deploy the WAR to one of the following runtimes: [Jakarta EE 10 Compatible Products](https://jakarta.ee/compatibility/certification/10/)
### Certified Runtimes
#### Full Profile
- [Glassfish Full](https://download.eclipse.org/ee4j/glassfish/glassfish-7.0.0-M8.zip)
- [Payara Full](https://github.com/payara/Payara/releases/download/payara-server-6.2022.1.Alpha4/payara-6.2022.1.Alpha4.zip)
- [Wildfly](https://github.com/wildfly/wildfly/releases/download/27.0.0.Alpha5/wildfly-27.0.0.Alpha5.zip)
#### Web Profile
- [Glassfish Web](https://download.eclipse.org/ee4j/glassfish/web-7.0.0-M8.zip)
- [Wildfly](https://github.com/wildfly/wildfly/releases/download/27.0.0.Alpha5/wildfly-27.0.0.Alpha5.zip)
### Maven Plugins
#### TomEE (Web Profile only)
- Run: `mvn tomee:run`
- Application Path: http://localhost:8080/appname
#### Wildfly (Not Working)
- Run: `mvn wildrfly:run`
- Application Path: http://localhost:8080/appname
#### Open Liberty (Not Jakarta EE 10 Compatible)
- Run: `mvn liberty:dev`
- Application Path: http://localhost:9080/appname
  - Attach Debugger to Port: 7777 
- Open Liberty Configuration
  - Configuration Center:  http://localhost:9080/adminCenter/
  - Username: admin
  - Password: admin
# Platform Overview
- [Jakarta EE Web Profile 10](https://jakarta.ee/specifications/webprofile/10/jakarta-webprofile-spec-10.0.pdf)
- [Jakarta EE Platform 10](https://jakarta.ee/specifications/platform/10/jakarta-platform-spec-10.0.pdf)
- [Jakarta Servlet 6](https://jakarta.ee/specifications/servlet/6.0/jakarta-servlet-spec-6.0.pdf)

## Important Specifications
- [**Context Dependency Injection**](https://jakarta.ee/specifications/cdi/4.0/jakarta-cdi-spec-4.0.pdf)
- [**Persistence**](https://jakarta.ee/specifications/persistence/3.1/jakarta-persistence-spec-3.1.pdf)
- [**RESTful Web Services**](https://jakarta.ee/specifications/restful-ws/3.1/jakarta-restful-ws-spec-3.1.html) ([PDF](https://jakarta.ee/specifications/restful-ws/3.1/jakarta-restful-ws-spec-3.1.pdf))
- [**Faces**](https://jakarta.ee/specifications/faces/4.0/jakarta-faces-4.0.pdf)
- [Bean Validation](https://jakarta.ee/specifications/bean-validation/3.0/jakarta-bean-validation-spec-3.0.pdf)
- [Security](https://jakarta.ee/specifications/security/2.0/jakarta-security-spec-2.0.pdf)
- [JSON Processing](https://jakarta.ee/specifications/jsonp/2.0/apidocs/)
- [Enterprise Beans (_Schedule/Asynchronous_)](https://jakarta.ee/specifications/enterprise-beans/4.0/jakarta-enterprise-beans-spec-core-4.0.pdf)
## Changelog
### Java EE 11 / Jakarta EE 10
First Release: 2022 \
Java-Base: 11
- [Servlet (5.0 --> 6.0)](https://jakarta.ee/specifications/servlet/6.0/)
- [Context Dependency Injection (3.0 --> 4.0)](https://jakarta.ee/specifications/cdi/4.0/)
- [Persistence (3.0 --> 3.1)](https://jakarta.ee/specifications/persistence/3.1/)
- [RESTful Web Services (3.0 --> 3.1)](https://jakarta.ee/specifications/restful-ws/3.1/)
- [Faces (3.0 --> 4.0)](https://jakarta.ee/specifications/faces/4.0/)
- [Bean Validation 3.0](https://jakarta.ee/specifications/bean-validation/3.0/)
- [Security (2.0 --> 3.0)](https://jakarta.ee/specifications/security/3.0/)
- [JSON Processing (2.0 --> 2.1)](https://jakarta.ee/specifications/jsonp/2.1/)
- [Enterprise Beans 4.0](https://jakarta.ee/specifications/enterprise-beans/4.0/)

### Java EE 9 / Jakarta EE 9
First Release: 2020 \
Java-Base: 8
- Identical to Java EE 8 / Jakarta EE 8. Only namespace changed from _javax_ to _jakarta_.
- [Servlet 5.0](https://jakarta.ee/specifications/servlet/5.0/)
- [Context Dependency Injection 3.0](https://jakarta.ee/specifications/cdi/3.0/)
- [Persistence 3.0](https://jakarta.ee/specifications/persistence/3.0/)
- [RESTful Web Services 3.0](https://jakarta.ee/specifications/restful-ws/3.0/)
- [Server Faces 3.0](https://jakarta.ee/specifications/faces/3.0/)
- [Bean Validation 3.0](https://jakarta.ee/specifications/bean-validation/3.0/)
- [Security 2.0](https://jakarta.ee/specifications/security/2.0/)
- [JSON Processing 2.0](https://jakarta.ee/specifications/jsonp/2.0/)
- [Enterprise Beans 4.0](https://jakarta.ee/specifications/enterprise-beans/4.0/)

<details>
<summary>Click to expand History</summary>

### Java EE 8 / Jakarta EE 8
First Release: 2017 \
Java-Base: 8
- Servlet 4.0
- Context Dependency Injection 2.0
- Persistence 2.2
- RESTful Web Services 2.1
- Server Faces 2.3
- Bean Validation 2.0
- **NEW** Security 1.0
- JSON Processing 1.1
- Enterprise Beans 3.2
### Java EE 7
First Release: 2013 \
Java-Base: 7
- Servlet 3.1
- Context Dependency Injection 1.1
- Persistence 2.1
- RESTful Web Services 2.0
- Server Faces 2.2
- Bean Validation 1.1
- JSON Processing 1.0
- Enterprise Beans 3.2

### Java EE 6
First Release: 2009 \
Java-Base: 6
- Servlet 3.0

### Java EE 5
First Release: 2006 \
Java-Base: 5
- Servlet 2.5
---
</details>

## Jakarta Persistence
### Using in Production
Configure Datasource in the Runtime. See OpenLiberty example:
- src/main/liberty/config/server.xml
### Remove Jakarta Persistence
If not needed delete file:
- src/main/resources/META-INF/persistence.xml

and Package:
- com.demo.appname.api.ping

## Jakarta Rest
### Remove Jakarta Rest
If not needed delete class:
- com.demo.appname.api.ApiApplicationConfig

and packages:
- com.demo.appname.api.health
- com.demo.appname.api.ping.boundary

## Jakarta Server Faces
### Using in Production
Before using in Production change "src/main/webapp/WEB-INF/web.xml"

remove:
```XML
    <context-param>
        <param-name>jakarta.faces.PROJECT_STAGE</param-name>
        <param-value>Development</param-value>
    </context-param>
```
### Remove Jakarta Server Faces
If not needed delete files:
- src/main/webapp/WEB-INF/web.xml
- src/main/webapp/index.xhtml
- src/main/webapp/welcome.xml

and Package:
- com.demo.appname.view

---
## Additional
## Primefaces Components
- [Primefaces Showcase](https://www.primefaces.org/showcase/ui/input/oneMenu.xhtml?jfwid=f17ce)
- [Primefaces Documentation](https://primefaces.github.io/primefaces/11_0_0/#/gettingstarted/helloworld)
### Add
If needed add to pom.xml:
```XML
<dependency>
    <groupId>org.primefaces</groupId>
    <artifactId>primefaces</artifactId>
    <version>11.0.0</version>
    <classifier>jakarta</classifier>
</dependency>
```
and add to xhtml:

`xmlns:p="http://primefaces.org/ui"`