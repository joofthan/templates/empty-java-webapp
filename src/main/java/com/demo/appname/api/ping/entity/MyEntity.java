package com.demo.appname.api.ping.entity;

import jakarta.persistence.*;

@NamedQueries(
        @NamedQuery(name = "MyEntity.findAll", query = "select e from MyEntity e")
)
@Entity
public class MyEntity {

    @Id
    @GeneratedValue
    private Integer id;
    private String name;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
