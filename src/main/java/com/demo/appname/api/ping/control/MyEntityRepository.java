package com.demo.appname.api.ping.control;

import com.demo.appname.api.ping.entity.MyEntity;
import jakarta.enterprise.context.RequestScoped;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;

import java.util.List;

@Transactional
@RequestScoped
public class MyEntityRepository {

    @PersistenceContext
    EntityManager em;

    public MyEntity merge(MyEntity e){
        return em.merge(e);
    }

    public MyEntity findById(int id){
        return em.find(MyEntity.class,id);
    }

    public List<MyEntity> findAll(){
        return em.createNamedQuery("MyEntity.findAll", MyEntity.class).getResultList();
    }
}
