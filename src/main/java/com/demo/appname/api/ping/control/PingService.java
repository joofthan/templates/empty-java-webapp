package com.demo.appname.api.ping.control;

import com.demo.appname.api.ping.entity.MyEntity;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

import java.util.List;

@ApplicationScoped
public class PingService {

    @Inject
    MyEntityRepository repo;

    public List<MyEntity> ping() {
        MyEntity e = new MyEntity();
        e.setName("hello world");
        repo.merge(e);
        return repo.findAll();
    }
}
