package com.demo.appname.api.ping.boundary;

import com.demo.appname.api.ping.control.PingService;
import com.demo.appname.api.ping.entity.MyEntity;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

import java.util.List;


@Path("ping")
public class PingResource {

    @Inject
    PingService pingService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<MyEntity> ping(){
        return pingService.ping();
    }
}
