package com.demo.appname.view.hello.boundary;

import com.demo.appname.view.hello.entity.Pet;
import jakarta.enterprise.context.*;
import jakarta.inject.*;

import java.io.Serializable;

@Named("helloView")
@SessionScoped
public class HelloView implements Serializable {
	private Pet pet;

	public Pet getPet() {
		if(pet == null){
			pet = new Pet();
			pet.setName( "world");
		}
		return pet;
	}

	public void setPet(Pet pet) {
		this.pet = pet;
	}
}