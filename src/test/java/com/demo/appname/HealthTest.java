package com.demo.appname;

import com.demo.appname.api.health.HealthResource;
import com.joofthan.enterprise.cdi.EnableInject;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@EnableInject
public class HealthTest {

    @Inject
    HealthResource healthResource;

    @Test
    void test(){
        assertEquals("UP", healthResource.get());
    }
}
