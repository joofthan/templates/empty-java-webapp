package com.demo.appname;

import com.demo.appname.api.ping.boundary.PingResource;
import com.demo.appname.api.ping.control.MyEntityRepository;
import com.demo.appname.api.ping.entity.MyEntity;
import com.joofthan.enterprise.cdi.EnableInject;
import com.joofthan.enterprise.cdi.UseForInject;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@EnableInject
public class PingTest {

    @Inject
    PingResource pingResource;

    @UseForInject
    MyEntityRepository myEntityRepository = mock(MyEntityRepository.class);

    @Test
    void should_returnNoEntities_when_databaseIsEmpty(){
        given_entities_in_database(0);
        when_ping_request();
        then_ping_response_contains_entities(0);
        and_repo_merge_was_called_at_least_once();
    }

    @Test
    void should_returnOneEntity_when_databaseHasOne(){
        given_entities_in_database(1);
        when_ping_request();
        then_ping_response_contains_entities(1);
        and_repo_merge_was_called_at_least_once();
    }

    @Test
    void should_returnMultipleEntity_when_databaseHasMultiple(){
        given_entities_in_database(2);
        when_ping_request();
        then_ping_response_contains_entities(2);
        and_repo_merge_was_called_at_least_once();
    }


    //DSL
    private List<MyEntity> pingResponse;
    private void given_entities_in_database(int number) {
        List<MyEntity> li = new ArrayList<>();
        for (int i = 0; i < number; i++) {
            li.add(new MyEntity());
        }
        when(myEntityRepository.findAll()).thenReturn(li);
    }

    private void when_ping_request() {
        pingResponse = pingResource.ping();
    }

    private void then_ping_response_contains_entities(int numberOfEntities) {
        assertEquals(numberOfEntities, pingResponse.size());
    }

    private void and_repo_merge_was_called_at_least_once() {
        verify(myEntityRepository).merge(any());
    }

}
